const nconf = require('nconf');
const path = require('path');

module.exports = {

  get: (key) => {
    return nconf.get(key);
  },

  init: () => {
    nconf.argv().env();
    nconf.file({file: path.resolve(__dirname, '../config/config.json')});
  }

};

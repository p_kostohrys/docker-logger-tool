
const dockerLogsController = require('../controllers/docker-logs-controller');

module.exports = function (app) {
  app.route('/findByContainerName/:containerName')
    .get(dockerLogsController.getLogsByContainerName);
  app.route('/findByContainerNameAndRows/:containerName/:rows')
    .get(dockerLogsController.getLogsByContainerNameAndRows);
  app.route('/findByContainerNameAndText/:containerName')
    .post(dockerLogsController.getLogsByContainerNameAndText);
};

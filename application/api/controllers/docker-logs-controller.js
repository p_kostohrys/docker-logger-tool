'use strict';
const createError = require('http-errors');
const logStorageResolver = require('../../lib/storage/log-storage-resolver');
const logStorage = logStorageResolver.resolve();
const logger = require('../../lib/util/logger-wrapper');

module.exports = {
  getLogsByContainerName: (req, res) => {
    logStorage.readByContainerName(req.params.containerName)
      .then(data => res.json(data))
      .catch(error => res.send(error));
  },
  getLogsByContainerNameAndRows: (req, res) => {
    logStorage.readByLines(req.params.rows, req.params.containerName)
      .then(data => res.json(data))
      .catch(error => res.send(error));
  },
  getLogsByContainerNameAndText: (req, res) => {
    if (!req.body.text || typeof (req.body.text) !== 'string') res.send(createError(400));
    logStorage.readByText(req.body.text, req.params.containerName)
      .then(data => res.json(data))
      .catch(error => logger.error(error));
  }
};

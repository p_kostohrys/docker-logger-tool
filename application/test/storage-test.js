const expect = require('chai').expect;
const sinon = require('sinon');
const configWrapper = require('../config/config-wrapper');
configWrapper.init();

const logStorageResolver = require('../lib/storage/log-storage-resolver');
const DiscLogStorage = require('../lib/storage/disc-log-storage');
const MongoDBLogStorage = require('../lib/storage/mongodb-log-storage');

const testContainerName = 'TestContainer';
const TestLogDB = 'TestLogDB';

describe('storage layer', function () {
  describe('log-storage-resolver', function () {
    it('"resolve" default strategy must be "DiscLogStorage"', function () {
      const strategy = logStorageResolver.resolve();
      expect(strategy).to.be.an.instanceof(DiscLogStorage);
    });
    it('"resolve" must return strategy depend for config file parameter', function () {
      const stub = sinon.stub(configWrapper, 'get')
        .returns('test');
      const strategy = logStorageResolver.resolve();
      expect(strategy).to.be.an.instanceof(DiscLogStorage);
      stub.restore();
    });
  });

  describe('disc-log-storage', function () {
    it('"readByContainerName"', async function () {
      const doc = await new DiscLogStorage(TestLogDB).readByContainerName('./test/testdoc');
      expect(doc)
        .to.be.an('array')
        .to.have.lengthOf(8);
    });
    it('"readByLines"', async function () {
      const doc = await new DiscLogStorage().readByLines(2, './test/testdoc');
      expect(doc)
        .to.be.an('array')
        .to.have.lengthOf(2);
      expect(doc[0]).to.have.string(`Hey you, dont help them to bury the light`);
      expect(doc[1]).to.have.string(`Don't give in without a fight.`);
    });
    it('"readByText"', async function () {
      const doc = await new DiscLogStorage().readByText('you', 'testdoc', './log/container/test');
      expect(doc)
        .to.be.an('array')
        .to.have.lengthOf(5);
    });
  });

  describe('mongodb-log-storage', function () {
    it('check connection', async function () {
      const strategy = new MongoDBLogStorage(TestLogDB);
      let connection;
      try {
        connection = await strategy.connection;
      } catch (e) {
        return Promise.reject(e);
      }
      expect(connection.constructor.name).to.be.equal('Mongoose');
    });
    it('working with documents', async function () {
      const strategy = new MongoDBLogStorage(TestLogDB);
      const randomString = (Math.random() + 1).toString(36).substring(7);

      // write to db
      let result;
      result = await strategy.write(randomString, testContainerName);
      const id = result._id;
      expect(id).to.be.an('object');

      // test read //

      // by id
      let doc = await strategy.readById(id, testContainerName);
      expect(doc)
        .to.be.an('object')
        .to.include.all.keys('_id', 'text', 'time');
      expect(doc._id.toString()).to.be.equal(id.toString());
      expect(doc.text).to.be.equal(randomString);
      expect(new Date(doc.time)).to.be.below(new Date(), 'time should be below that current time');

      // by container
      const docs = await strategy.readByContainerName(testContainerName);
      expect(docs).to.be.an('array');
      docs.forEach(doc => expect(doc).to.include.all.keys('_id', 'text', 'time'));

      // find text in container`s log
      const docsByText = await strategy.readByText(randomString, testContainerName);
      expect(docsByText)
        .to.be.an('array');
      docsByText.forEach(doc => {
        expect(doc).to.include.all.keys('_id', 'text', 'time');
        expect(doc.text).to.have.string(randomString);
      });

      // remove by id
      await strategy.removeById(id, testContainerName);
      const removedDoc = await strategy.readById(id, testContainerName);
      expect(removedDoc).to.be.null; /* eslint no-unused-expressions: 0 */

      // generate 15 documents
      for (let i = 0; i < 15; i++) { await strategy.write(`${randomString}${i}`, testContainerName); }

      // last 10 rows in container`s log
      const docsByLines = await strategy.readByLines(10, testContainerName);
      expect(docsByLines)
        .to.be.an('array')
        .to.have.lengthOf(10);
      docsByLines.forEach(doc => expect(doc).to.include.all.keys('_id', 'text', 'time'));
      expect(new Date(docsByLines[9].time)).to.be.below(new Date(docsByLines[0].time), 'descending by time');

      // clear all
      await strategy.clear(testContainerName);
      const clean = await strategy.readByContainerName(testContainerName);
      expect(clean)
        .to.be.an('array')
        .to.have.lengthOf(0);
    });
  });
});

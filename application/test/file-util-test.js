const expect = require('chai').expect;
const { find } = require('../lib/util/file-util');

const word = 'you';

describe('file-util', function () {
  describe('"find" function', function () {
    it('"find" should be a function that returns a Promise', function () {
      expect(find).to.be.a('function');
      expect(find(word, './log/container/test', 'testdoc')).to.be.an.instanceof(Promise);
    });
    it('"find" should be resolved with array with 1 item', async function () {
      const result = await find(word, './log/container/test', 'testdoc');
      expect(result)
        .to.be.an('array')
        .to.have.lengthOf(1);
    });
    it('"find" should return correct object', async function () {
      const result = await find(word, './log/container/test', 'testdoc');
      expect(result[0]).to.be.an('object');
      const { matches, count, line } = result[0];
      expect(matches)
        .to.be.an('array')
        .to.have.lengthOf(5)
        .to.include(word);
      expect(count).to.be.equal(5);
      expect(line)
        .to.be.an('array')
        .to.have.lengthOf(5);
      expect(line.filter(row => row.indexOf(word) !== -1)).to.have.lengthOf(5, 'each row should contain a word');
    });
  });
});

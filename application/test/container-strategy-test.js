const expect = require('chai').expect;
const sinon = require('sinon');
const configWrapper = require('../config/config-wrapper');

const dockerService = require('../lib/service/docker-service');

const containerStrategyResolver = require('../lib/container/container-strategy-resolver');
const AllContainersStrategy = require('../lib/container/strategy/all-containers-strategy');
const NameContainersStrategy = require('../lib/container/strategy/name-based-containers-strategy');
const ImageContainersStrategy = require('../lib/container/strategy/image-based-containers-strategy');

describe('container strategy', function () {
  describe('container-strategy-resolver', function () {
    it('"resolve" must return ALL', function () {
      testResolve('ALL', AllContainersStrategy);
    });
    it('"resolve" must return IMAGE-BASED', function () {
      testResolve('IMAGE-BASED', ImageContainersStrategy);
    });
    it('"resolve" must return NAME-BASED', function () {
      testResolve('NAME-BASED', NameContainersStrategy);
    });

    function testResolve (strategyType, instance) {
      const stub = sinon.stub(configWrapper, 'get')
        .returns(strategyType);
      const strategy = containerStrategyResolver.resolve();
      expect(strategy).to.be.an.instanceof(instance);
      stub.restore();
    }
  });
  describe('all-containers-strategy', function () {
    it('"get"', async function () {
      const containerMocks = [{Image: 'test'}, {Image: 'test2'}];
      const stub = sinon.stub(dockerService, 'containers')
        .returns(containerMocks);
      const containers = await new AllContainersStrategy().get();
      expect(containers)
        .to.be.an('array')
        .to.have.lengthOf(2);
      stub.restore();
    });
  });
  describe('image-based-containers-strategy', function () {
    it('"get"', async function () {
      const containerMocks = [{Image: 'test'}, {Image: 'docker-test'}, {Image: 'docker-logger'}];
      const stub = sinon.stub(dockerService, 'containers')
        .returns(containerMocks);
      const regexpStub = sinon.stub(configWrapper, 'get')
        .returns('docker');
      const containers = await new ImageContainersStrategy().get();
      expect(containers)
        .to.be.an('array')
        .to.have.lengthOf(2);
      stub.restore();
      regexpStub.restore();
    });
  });
  describe('name-based-containers-strategy', function () {
    it('"get"', async function () {
      const containerMocks = [{Image: 'test'}, {Image: 'docker-test'}, {Image: 'docker-logger'}];
      const stub = sinon.stub(dockerService, 'containers')
        .returns(containerMocks);
      const regexpStub = sinon.stub(configWrapper, 'get')
        .returns('docker');

      const containers = await new ImageContainersStrategy().get();
      expect(containers)
        .to.be.an('array')
        .to.have.lengthOf(2);

      stub.restore();
      regexpStub.restore();
    });
  });
});

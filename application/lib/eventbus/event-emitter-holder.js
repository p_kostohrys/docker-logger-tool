const events = require('events');
const em = new events.EventEmitter();

/**
 * Holder hor events emitter
 * @module eventbus/event-emitter-holder
 */
module.exports = em;

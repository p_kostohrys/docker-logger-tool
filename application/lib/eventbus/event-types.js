/**
 * Enum with event types
 * @module eventbus/event-types
 */
module.exports = Object.freeze({
  CONTAINER_START: 'CONTAINER_START',
  CONTAINER_STOP: 'CONTAINER_REMOVED',
  RECEIVE_ATTACH_MESSAGE: 'RECEIVE_ATTACH_MESSAGE'
});

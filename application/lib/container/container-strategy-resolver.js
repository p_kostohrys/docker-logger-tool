/**
 * Using appropriate container strategy
 * @module container/container-strategy-resolver
 */

const AllContainersStrategy = require('./strategy/all-containers-strategy');
const NameContainersStrategy = require('./strategy/name-based-containers-strategy');
const ImageContainersStrategy = require('./strategy/image-based-containers-strategy');
const configWrapper = require('../../config/config-wrapper');

module.exports = {

  /**
     * Returns selected container strategy
     * @returns {object} - Selected strategy
     */
  resolve: function () {
    const containerStrategy = configWrapper.get('containerStrategy');
    switch (containerStrategy) {
      case 'ALL' :
        return new AllContainersStrategy();
      case 'NAME-BASED' :
        return new NameContainersStrategy();
      case 'IMAGE-BASED' :
        return new ImageContainersStrategy();
      default :
        return new AllContainersStrategy();
    }
  }
};

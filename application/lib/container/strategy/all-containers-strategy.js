const dockerService = require('../../service/docker-service');

/**
 * @classdesc Listen all containers
 */
class AllContainersStrategy {
  /**
     * get all containers
     * @returns {Promise<array>}
     */
  async get () {
    return await dockerService.containers();
  }

  /**
     * filter containers according to strategy
     * @returns {boolean} always return true because we log all containers
     */
  check () {
    return true;
  }
}

/**
 * exports
 * @type {AllContainersStrategy}
 */
module.exports = AllContainersStrategy;

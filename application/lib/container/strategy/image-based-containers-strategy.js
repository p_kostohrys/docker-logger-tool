const configWrapper = require('../../../config/config-wrapper');
const dockerService = require('../../service/docker-service');

/**
 * @classdesc Listen containers based on image
 */
class ImageBasedContainersStrategy {
  constructor () {
    this.regexp = configWrapper.get('containerStrategyConfig:IMAGE-BASED:regexp');
  }

  /**
     * get all filtered containers
     * @returns {Promise<array>}
     */
  async get () {
    const containers = await dockerService.containers();
    return containers.filter(c => match(c, this.regexp));
  }

  /**
     * filter containers according to strategy
     * @param container
     * @returns {boolean}
     */
  check (container) {
    return match(container, this.regexp);
  }
}

module.exports = ImageBasedContainersStrategy;

function match (container, regexp) {
  const matches = container.Image.match(regexp);
  if (matches === null) {
    return false;
  }
  return matches[0] !== null;
}

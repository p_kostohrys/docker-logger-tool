const dockerService = require('../../service/docker-service');
const configWrapper = require('../../../config/config-wrapper');

/**
 * @classdesc Listen containers based on name
 */
class NameBasedContainersStrategy {
  constructor () {
    this.regexp = configWrapper.get('containerStrategyConfig:NAME-BASED:regexp');
  }

  /**
     * get all filtered containers
     * @returns {Promise<array>}
     */
  async get () {
    const containers = await dockerService.containers();
    return containers.filter(c => {
      return match(c, this.regexp);
    });
  }

  /**
     * filter containers according to strategy
     * @param container
     * @returns {boolean}
     */
  check (container) {
    return match(container, this.regexp);
  }
}

module.exports = NameBasedContainersStrategy;

function match (container, regexp) {
  return container.Names.some(n => {
    const matches = n.match(regexp);
    return matches && matches[0];
  });
}

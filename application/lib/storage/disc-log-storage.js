const LogStorage = require('./log-storage');

const fs = require('fs');
const util = require('util');
const fileUtil = require('../util/file-util');
const readFile = util.promisify(fs.readFile);
const path = require('path');

const dir = path.resolve(__dirname, '../../log/container');

let created = false;

/**
 * @classdesc Disc implementation of LogStorage
 */
class DiscLogStorage extends LogStorage {
  /**
     * write text to log storage
     * @param {string} text
     * @param {string} containerName
     * @returns {Promise<Object>}
     */
  write (text, containerName) {
    // TODO : optimize method, not check each time directory
    // TODO : doesnt work for window
    if (!created && !fs.existsSync(dir)) {
      fs.mkdirSync(dir);
      created = true;
    }
    fs.writeFile(dir + '/' + fileName(containerName), text, {'flag': 'a'}, function (err) {
      if (err) {
        return console.error(err);
      }
    });
  }

  /**
     * read logs of specified container that contain specified text
     * @param {string} text
     * @param {string} containerName
     * @returns {Promise<Array>}
     */
  async readByContainerName (containerName) {
    const data = await readFile(dir + '/' + fileName(containerName), 'utf8');
    return data.toString().split(/\n/);
  }

  /**
     * read last rows from log storage of specified container
     * @param {number} rows - number of rows to return
     * @param {string} containerName
     * @returns {Promise<Array>}
     */
  async readByLines (rows, containerName) {
    const data = await readFile(dir + '/' + fileName(containerName), 'utf8');
    return data.toString().split(/\n/).slice(-rows);
  }

  /**
     * read logs of specified container that contain specified text
     * @param {string} text
     * @param {string} containerName
     * @returns {Promise<Array>}
     */
  async readByText (text, containerName, path = dir) {
    let results = await fileUtil.find(text, path, fileName(containerName));
    return results.length ? results[0].line : [];
  }
}

function fileName (containerName) {
  return containerName + '.log';
}

/**
 * @type {DiscLogStorage}
 */
module.exports = DiscLogStorage;

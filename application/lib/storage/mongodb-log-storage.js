const LogStorage = require('./log-storage');

const configWrapper = require('../../config/config-wrapper');
const mongoose = require('mongoose');
const { Schema } = mongoose;

/**
 * @classdesc MongoDB implementation of LogStorage
 */
class MongoDBLogStorage extends LogStorage {
  /**
     * @param [string] databaseName - if omit config value will be used
     */
  constructor (databaseName) {
    super();

    const { url, port, database } = configWrapper.get('mongodb');

    this.mongoose = mongoose;
    this.connection = mongoose.connect(`mongodb://${url}:${port}/${databaseName || database}`);

    this.LogSchema = new Schema({
      text: String,
      time: { type: Date, default: Date.now }
    });
    this.models = {};
  }

  getModel (name) {
    if (!this.models[name]) {
      this.models[name] = mongoose.model(`${name}log`, this.LogSchema);
    }
    return this.models[name];
  }

  /**
     * write text to log storage
     * @param {string} text
     * @param {string} containerName
     * @returns {Promise<Object>}
     */
  write (text, containerName) {
    const Model = this.getModel(containerName);
    const instance = new Model({ text });
    return instance.save();
  }

  /**
     * read logs of specified container that contain specified text
     * @param {string} text
     * @param {string} containerName
     * @returns {Promise<Array>}
     */
  async readByContainerName (containerName) {
    const Model = this.getModel(containerName);
    return (await Model.find()).map(model => model._doc);
  }

  /**
     * read last rows from log storage of specified container
     * @param {number} rows - number of rows to return
     * @param {string} containerName
     * @returns {Promise<Array>}
     */
  async readByLines (rows, containerName) {
    const Model = this.getModel(containerName);
    return (await Model.find().sort({'time': -1}).limit(rows).exec()).map(model => model._doc);
  }

  /**
     * read logs of specified container that contain specified text
     * @param {string} text
     * @param {string} containerName
     * @returns {Promise<Array>}
     */
  async readByText (text, containerName) {
    const Model = this.getModel(containerName);
    return (await Model.find({ text: new RegExp(text) })).map(model => model._doc);
  }

  /**
     * read log by id and container
     * @param {ObjectId} id
     * @param {string} containerName
     * @returns {Promise<Object>}
     */
  async readById (id, containerName) {
    const Model = this.getModel(containerName);
    const data = (await Model.findById(id));
    return data ? data._doc : null;
  }

  /**
     * remove log by id and container
     * @param {ObjectId} id
     * @param {string} containerName
     * @returns {Promise<Object>}
     */
  async removeById (id, containerName) {
    const Model = this.getModel(containerName);
    return (await Model.findByIdAndRemove(id));
  }

  /**
     * clear all logs for specified container
     * @param {string} containerName
     * @returns {Promise<Object>}
     */
  async clear (containerName) {
    const Model = this.getModel(containerName);
    return (await Model.remove({}));
  }
}

module.exports = MongoDBLogStorage;

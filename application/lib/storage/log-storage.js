const ABSTRACT_METHOD_ERROR = `Abstract method not implemented`;
const ABSTRACT_CLASS_ERROR = `Can't instantiate abstract class`;

/**
 * @classdesc Abstract class of log storage
 * @abstract
 */
class LogStorage {
  constructor () {
    if (this.constructor === LogStorage) {
      throw (new Error(ABSTRACT_CLASS_ERROR));
    }
  }

  /**
     * write text to log storage
     * @param {string} text
     * @param {string} containerName
     * @abstract
     */
  write (text, containerName) {
    if (this.constructor === LogStorage) {
      throw (new Error(ABSTRACT_METHOD_ERROR));
    }
  }

  /**
     * read from log storage all logs of specified container
     * @param {string} containerName
     * @abstract
     */
  readByContainerName (containerName) {
    if (this.constructor === LogStorage) {
      throw (new Error(ABSTRACT_METHOD_ERROR));
    }
  }

  /**
     * read last rows from log storage of specified container
     * @param {number} rows - number of rows to return
     * @param {string} containerName
     * @abstract
     */
  readByLines (rows, containerName) {
    if (this.constructor === LogStorage) {
      throw (new Error(ABSTRACT_METHOD_ERROR));
    }
  }

  /**
     * read logs of specified container that contain specified text
     * @param {string} text
     * @param {string} containerName
     * @abstract
     */
  readByText (text, containerName) {
    if (this.constructor === LogStorage) {
      throw (new Error(ABSTRACT_METHOD_ERROR));
    }
  }
}

/**
 * LogStorage class
 * @type {LogStorage}
 */
module.exports = LogStorage;

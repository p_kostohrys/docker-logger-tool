/**
 * Using appropriate log storage
 * @module storage/log-storage-resolver
 */

const DiscLogStorage = require('./disc-log-storage');
const MongoDBLogStorage = require('./mongodb-log-storage');
const configWrapper = require('../../config/config-wrapper');
const logger = require('../util/logger-wrapper');

/**
 * @type {{resolve: module.exports.resolve}}
 */
module.exports = {
  /**
     * Returns selected log storage
     * @returns {LogStorage}
     */
  resolve: function () {
    const logStorageType = configWrapper.get('logStorageType');
    logger.debug('Resolve log storage type ' + logStorageType);
    switch (logStorageType) {
      case 'DISC' :
        return new DiscLogStorage();
      case 'MONGODB' :
        return new MongoDBLogStorage();
      default :
        return new DiscLogStorage();
    }
  }
};

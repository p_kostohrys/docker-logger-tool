/**
 * Listen containers events
 * @module listener/docker-event-listener
 */

const DockerEvents = require('docker-events');
const Dockerode = require('dockerode');
const router = require('../event/docker-event-router');

const logger = require('../util/logger-wrapper');

const emitter = new DockerEvents({
  docker: new Dockerode()
});

module.exports = {

  /**
     * Start listen containers events
     */
  listen: function () {
    logger.info('Start event listener');
    emitter.start();

    emitter.on('connect', function () {
      logger.info('connected to docker api');
    });

    emitter.on('disconnect', function () {
      logger.info('disconnected to docker api; reconnecting');
    });

    emitter.on('_message', function (event) {
      logger.info(`my value: ${JSON.stringify(event)}`);
      router.route(event);
    });
  },

  /**
     * Stop listen containers events
     */
  stop: function () {
    logger.info('Stop event listener');
    emitter.stop();
  }

};

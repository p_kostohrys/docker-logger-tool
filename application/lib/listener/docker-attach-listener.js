/**
 * Listen start/stop events
 * @module listener/docker-attach-listener
 */

const em = require('../eventbus/event-emitter-holder');
const Modem = require('docker-modem/lib/modem');
const Container = require('dockerode').Container;
const eventTypes = require('../eventbus/event-types');
const dockerAttachService = require('../service/docker-attach-service');
const dockerService = require('../service/docker-service');
const containerStrategyResolver = require('../container/container-strategy-resolver');
const logger = require('../util/logger-wrapper');
const containerStrategy = containerStrategyResolver.resolve();

module.exports = {

  /**
   * Start listen start/stop events
   */
  listen: function () {
    em.addListener(eventTypes.CONTAINER_START, async function (data) {
      const container = await dockerService.containerInfo(data.id);
      if (!container) {
        logger.error(`Cant find container by id ${data.id}`);
        return;
      }
      logger.info('Container start: ' + container.Image);
      if (!containerStrategy.check(container)) {
        logger.info(`Ignore container, not apply for strategy ${containerStrategy}`);
        return;
      }
      dockerAttachService.attachToContainer(new Container(new Modem(), data.id));
    });
    em.addListener(eventTypes.CONTAINER_STOP, function (data) {
      logger.info('Container stop: ' + data.id);
    });
  }

};

/**
 * Listen logs
 * @module listener/log-storage-listener
 */

const eventTypes = require('../eventbus/event-types');
const em = require('../eventbus/event-emitter-holder');
const logStorageResolver = require('../storage/log-storage-resolver');

const logger = require('../util/logger-wrapper');

module.exports = {
  /**
   * Add listener for logs
   */
  listen: function () {
    em.addListener(eventTypes.RECEIVE_ATTACH_MESSAGE, function (data) {
      logger.debug('Receive attach message: ' + data.message);
      logStorageResolver.resolve().write(data.message, data.container.Image);
    });
  }
};

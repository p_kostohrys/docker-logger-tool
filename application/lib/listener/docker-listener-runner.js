const eventService = require('./docker-event-listener');
const logStorageListener = require('./log-storage-listener');
const dockerAttachListener = require('./docker-attach-listener');

module.exports = {

  run: function () {
    dockerAttachListener.listen();
    logStorageListener.listen();
    eventService.listen();
  }

};

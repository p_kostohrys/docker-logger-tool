'use strict';
/**
 * Module for finding files
 * @module util/file-utils
 */

const fs = require('fs');
const path = require('path');
const { promisify } = require('util');

const readdir = promisify(fs.readdir);
const stat = promisify(fs.stat);
const readFile = promisify(fs.readFile);

/**
 * @param {object} data Search params
 * @returns {function(string)}
 */
const searchFile = data => content => {
  const { filename } = data;
  const match = content.match(data.regex);
  const lines = content.match(data.lineRegEx);

  return { filename, lines, match };
};

/**
 * build regex for file filter
 * @param [string|RegExp] fileFilter
 * @returns {RegExp}
 */
function getFileFilter (fileFilter) {
  switch (typeof fileFilter) {
    case 'string':
      return new RegExp(fileFilter);
    case 'undefined':
      return new RegExp('.');
    default:
      return fileFilter;
  }
}

/**
 * build regex for content
 * @param {object|string} pattern
 * @param {string} regex
 * @returns {RegExp}
 */
function getRegEx (pattern, regex) {
  let flags, term;

  if (typeof pattern === 'object' && pattern.flags) {
    term = pattern.term;
    flags = pattern.flags;
  } else {
    term = pattern;
    flags = 'g';
  }

  const grabLineRegEx = `(.*${term}.*)`;

  if (regex === 'line') {
    return new RegExp(grabLineRegEx, flags);
  }

  return new RegExp(term, flags);
}

/**
 * get files filtered by pattern
 * @param {object|string} pattern
 * @param {array} files
 * @returns {Promise<object[]>}
 */
async function getMatchedFiles (pattern, files) {
  return await Promise.all(files.map(async file => {
    return searchFile({
      regex: getRegEx(pattern),
      lineRegEx: getRegEx(pattern, 'line'),
      filename: file
    })(await readFile(file, 'utf-8'));
  }));
}

/**
 * parse results
 * @param {array} content
 * @return {array}
 */
function parseResults (content) {
  return content.map(item => {
    if (item && item.match !== null) {
      return {
        matches: item.match,
        count: item.match.length,
        line: item.lines
      };
    }
    return null;
  }).filter(item => !!item);
}

/**
 * check pattern against the path
 * @param {RegExp|string} filter
 * @param {string} name - File name
 * @returns {boolean}
 */
function compare (filter, name) {
  const str = path.basename(name);
  return (
    filter instanceof RegExp && filter.test(name) || typeof filter === 'string' && filter === str
  );
};

/**
 * get files filtered by parameter
 * @param {string} dir - root directiry
 * @param {RegExp|string} filter - filter
 * @returns {Promise<string[]>}
 */
async function getFiles (dir, filter) {
  const subdirs = await readdir(dir);
  const files = await Promise.all(subdirs.map(async (subdir) => {
    const res = path.resolve(dir, subdir);
    const isDir = (await stat(res)).isDirectory();
    if (isDir) {
      return getFiles(res);
    } else {
      return compare(filter, res) ? res : null;
    }
  }));
  return files.reduce((a, f) => f ? a.concat(f) : a, []);
}

/**
 * find files by cpecified criteria
 */
module.exports = {
  /**
     * Returns found content
     * @param pattern
     * @param directory
     * @param fileFilter
     * @returns {Promise<Array>}
     */
  find: async (pattern, directory, fileFilter) => {
    const files = await getFiles(directory, getFileFilter(fileFilter));
    const content = await getMatchedFiles(pattern, files);
    return parseResults(content);
  }
};

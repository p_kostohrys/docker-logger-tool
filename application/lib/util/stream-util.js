/**
 * Module for stream reading
 * @module util/stream-util
 */

/**
 * get stream data with error handling
 * @param {Stream} stream - input stream
 * @param {string} enc - encoding
 * @param {function} cb - callback
 * @returns {Promise<string>}
 */
function readStream (stream, enc, cb) {
  if (typeof enc === 'function') {
    cb = enc;
    enc = null;
  }
  cb = cb || function () {};

  return new Promise(function (resolve, reject) {
    stream.on('data', function (data) {
      const str = (typeof enc === 'string') ? data.toString(enc) : data.toString();
      resolve(str);
      cb(null, str);
    });
    stream.on('error', function (err) {
      reject(err);
      cb(err, null);
    });
  });
}

/**
 * Export
 * @type {readStream}
 */
module.exports = readStream;

/**
 * @module service/docker-attach-service
 */

const dockerService = require('./docker-service');
const Modem = require('docker-modem/lib/modem');
const Container = require('dockerode').Container;
const Stream = require('../util/stream-util');
const eventTypes = require('../eventbus/event-types');

const containerStrategyResolver = require('../container/container-strategy-resolver');

const em = require('../eventbus/event-emitter-holder');

const logger = require('../util/logger-wrapper');

module.exports = {
  /**
     * Attach to all containers and listen
     */
  attach: function () {
    logger.info('Attach to container and listen');
    const containerStrategy = containerStrategyResolver.resolve();
    containerStrategy.get()
      .then(function (containers) {
        containers.forEach(container => {
          attachToContainer(new Container(new Modem(), container.Id));
        });
      })
      .catch(console.error);
  },

  /**
     * Attach to specified container
     * @param container
     */
  attachToContainer: function (container) {
    attachToContainer(container);
  }
};

function attachToContainer (container) {
  container.attach({stream: true, stdout: true, stderr: true}, function (err, stream) {
    if (err) {
      logger.error(err);
      return;
    }
    logger.info('Start listen attach log stream for container ' + container.id);
    new Stream(stream, 'utf-8', function (err, message) {
      if (err) {
        logger.error(err);
      }
      dockerService.containerInfo(container.id).then(c => {
        if (c !== null) {
          em.emit(eventTypes.RECEIVE_ATTACH_MESSAGE, {message: message, container: c});
        }
      });
    });
  });
}

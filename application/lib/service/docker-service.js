/**
 * Docker interaction
 * @module service/docker-service
 */

const Dockerode = require('dockerode');

const docker = new Dockerode();

module.exports = {

  /**
  * Get running containers
  */
  containers: function () {
    return docker.listContainers();
  },

  /**
  * Get specified container
  * @param containerId
  * @returns {*}
  */
  container: function (containerId) {
    return docker.getContainer(containerId);
  },

  /**
   * Get container info
   * @param containerId
   * @returns {Promise<Array>}
   */
  containerInfo: async function (containerId) {
    const containers = await docker.listContainers();
    return containers.find(c => c.Id === containerId);
  }

};

/**
 * Route events
 * @module event/docker-event-router
 */

const eventTypes = require('../eventbus/event-types');
const logger = require('../util/logger-wrapper');
const em = require('../eventbus/event-emitter-holder');

module.exports = {

  /**
     * Route event to correct listener
     * @param event
     */
  route: function (event) {
    if (event.Type === 'container' && event.Action === 'start') {
      logger.info('Emit event that container start');
      em.emit(eventTypes.CONTAINER_START, event);
    }
    if (event.Type === 'container' && event.Action === 'stop') {
      logger.info('Emit event that container stop');
      em.emit(eventTypes.CONTAINER_STOP, event);
    }
  }

};

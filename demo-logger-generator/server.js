'use strict';

const path = require('path');
const LineReader = require('line-by-line');
class ReadFile {
  constructor () {
    this.filePath = path.resolve(__dirname, 'kobzar.txt');
    this.lr = new LineReader(this.filePath);
    this.readSong();
  }
  readSong () {
    const _this = this;
    this.lr.on('line', function (line) {
      console.log(line);
      _this.lr.pause();
      setTimeout(() => { _this.lr.resume(); }, 100);
    });
    this.lr.on('end', function () {
      console.log('==========================END KOBZAR==========================');
      console.log('=========================== REPEAT ===========================');
      _this.readSong();
    });
  }
}
new ReadFile();
